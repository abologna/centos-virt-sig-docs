# Introduction

Welcome to the Virtualization Special Interest Group (Virt-SIG)!

The Virt-SIG aims to deliver a user consumable full stack for virtualization technologies that want to work with the SIG.
This includes delivery, deployment, management, update and patch application (for full lifecycle management) of the
baseline platform when deployed in sync with a technology curated by the Virt-SIG.

The delivery stage would be executed via multiple online, updated RPM repositories to complement ISO based delivery
of install media, livecd/livedvd/livdusb media etc.

The SIG will also attempt to maintain user grade documentation around the technologies we curate, along with represent the
Virt-SIG at CentOS Dojos and related events in community and vendor neutral events.

The SIG will only work with opensource, redistributable software. However some of the code we want to build and ship might
not be mainline accepted as yet, and would be clearly indicated as such.
