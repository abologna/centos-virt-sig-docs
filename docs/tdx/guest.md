# Run a TD guest (VM)

Follow the instructions on this page to create and run a TD guest (VM) on a hypervisor host configured with TDX support (See [Configure a host](./host.md)).


## Create VM Disk Image

1. Download the x86_64 ISO installer to use as a TD guest(VM) image:

   - RHEL 9.2 or newer (See [RHEL 9.2 Release Notes](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html/9.2_release_notes/technology-previews#technology-previews-virtualization))

   - The latest CentOS Stream 9 x86_64 ISO from the [CentOS website](https://www.centos.org/centos-stream/)

    The example commands below use CentOS Stream 9.


2. Create a qcow2 format guest image, using with UEFI support. The example command below will take a `CentOS-Stream-9-latest-x86_64-dvd1.iso` file and create a `centos9.qcow2` file:

    ```bash
    #To create a qcow2
    qemu-img create centos9.qcow2 -f qcow2 20G

    #Start a VM and install ISO to the qcow2 image
    /usr/libexec/qemu-kvm -accel kvm -smp 8 -m 8G -cpu host \
    -drive file=centos9.qcow2,if=none,id=virtio-disk0 \
    -device virtio-blk-pci,drive=virtio-disk0 \
    -bios /usr/share/edk2/ovmf/OVMF.inteltdx.fd \
    -cdrom CentOS-Stream-9-latest-x86_64-dvd1.iso \
    -usb -device usb-tablet \
    -daemonize -vnc :1

    #To install vncviewer
    dnf install tigervnc

    #To get into the VM windows
    vncviewer :1
    #Kill this /usr/libexec/qemu-kvm process after installation
    ```


3. After creating a qcow2 with ISO, install kernel-tdx in guest

    ```bash
    #To create a non-TD guest
    /usr/libexec/qemu-kvm \
    -accel kvm -smp 16 -m 16G -cpu host -machine q35,kernel_irqchip=split \
    -drive file=/home/tdx/centos9.qcow2,if=none,id=virtio-disk0 \
    -device virtio-blk-pci,drive=virtio-disk0 \
    -device virtio-net-pci,netdev=nic0 -netdev user,id=nic0,hostfwd=tcp::10022-:22 \
    -bios /usr/share/edk2/ovmf/OVMF.inteltdx.fd \
    -serial stdio

    #Login to OS on the stdio terminal or open anoter terminal and "ssh -p 10022 user@localhost"
    #Add repo and install tdx-kernel
    echo "[tdx]
    name=CentOS TDX
    metalink=https://mirrors.centos.org/metalink?repo=centos-virt-sig-tdx-devel-\$releasever-stream&arch=\$basearch&protocol=https
    gpgcheck=0
    enabled=1" | sudo tee /etc/yum.repos.d/tdx.repo
    
    #Install TDX kernel
    dnf install kernel-tdx

    #Poweroff guest
    poweroff
    ```
## Configure and boot VM

A TD guest can be created and booted using `qemu-kvm` or `virsh` after the UEFI qcow2 image has been created.

### TD guest using QEMU

1. Boot a TD guest using qemu-kvm. If you prefer to use virsh, skip to the next section.

    ```
    /usr/libexec/qemu-kvm \
    -accel kvm \
    -m 4G -smp 1 \
    -name process=tdxvm,debug-threads=on \
    -cpu host \
    -object tdx-guest,id=tdx \
    -machine q35,hpet=off,kernel_irqchip=split,memory-encryption=tdx,memory-backend=ram1 \
    -object memory-backend-ram,id=ram1,size=4G,private=on \
    -nographic -vga none \
    -chardev stdio,id=mux,mux=on,signal=off -device virtio-serial -device virtconsole,chardev=mux \
    -bios /usr/share/edk2/ovmf/OVMF.inteltdx.fd \
    -serial chardev:mux \
    -nodefaults \
    -device virtio-net-pci,netdev=nic0 -netdev user,id=nic0,hostfwd=tcp::10022-:22 \
    -drive file=/home/tdx/centos9.qcow2,if=none,id=virtio-disk0 \
    -device virtio-blk-pci,drive=virtio-disk0
    ```

### TD guest using virsh

Alternatively, a TD guest can be created using virsh.
If you prefer to use qemu-kvm, go back to the previous section.  

1. Please make sure that current user is added to /etc/libvirt/qemu.conf, for example:  
```bash
# Add current user to /etc/libvirt/qemu.conf
echo "user = '$USER'
group = '$USER'
dynamic_ownership = 0
security_driver = 'none'
" | sudo tee -a /etc/libvirt/qemu.conf

# Restart libvirtd service
systemctl restart libvirtd
systemctl status libvirtd
```

2. Create the XML template file. Below is an example *td_guest.xml* with the created qcow2, replace the value of "source file" with the absolute path of your guest image.  

    ```xml
    <domain type='kvm' xmlns:qemu='http://libvirt.org/schemas/domain/qemu/1.0'>
        <name>my-td-guest</name>
        <memory unit='KiB'>4194304</memory>
        <memoryBacking>
            <source type="anonymous"/>
            <access mode="private"/>
        </memoryBacking>
        <vcpu placement="static">4</vcpu>
        <os>
            <type arch='x86_64' machine='q35'>hvm</type>
            <loader>/usr/share/edk2/ovmf/OVMF.inteltdx.fd</loader>
            <boot dev='hd'/>
        </os>
        <features>
            <acpi/>
            <apic/>
            <ioapic driver='qemu'/>
        </features>
        <clock offset='utc'>
            <timer name='hpet' present='no'/>
        </clock>
        <on_poweroff>destroy</on_poweroff>
        <on_reboot>restart</on_reboot>
        <on_crash>destroy</on_crash>
        <pm>
            <suspend-to-mem enable='no'/>
            <suspend-to-disk enable='no'/>
        </pm>
        <cpu mode='host-passthrough'>
            <topology sockets='1' cores='4' threads='1'/>
        </cpu>
        <devices>
            <emulator>/usr/libexec/qemu-kvm</emulator>
            <disk type="file" device="disk">
                <driver name="qemu" type="qcow2"/>
                <source file="/home/tdx/centos9.qcow2"/>
                <target dev="vda" bus="virtio"/>
            </disk>
            <console type='pty'>
                <target type='virtio' port='1'/>
            </console>
            <channel type='unix'>
                <source mode='bind'/>
                <target type='virtio' name='org.qemu.guest_agent.0'/>
            </channel>
        </devices>
        <allowReboot value='no'/>
        <launchSecurity type='tdx'>
            <policy>0x10000001</policy>
        </launchSecurity>
        <qemu:commandline>
            <qemu:arg value='-cpu'/>
            <qemu:arg value='host'/>
            <qemu:arg value='-device'/>
            <qemu:arg value='virtio-net-pci,netdev=nic0,bus=pcie.0,addr=0x5'/>
            <qemu:arg value='-netdev'/>
            <qemu:arg value='user,id=nic0,hostfwd=tcp::10022-:22'/>
        </qemu:commandline>
    </domain>
    ```

3. Enter the virsh shell

    ```
    # virsh
    Welcome to virsh, the virtualization interactive terminal.
    Type: 'help' for help with commands
    'quit' to quit
    ```

4. Create the VM using the XML template and start the VM

    ```
    virsh # define td_guest.xml
    Domain 'my-td-guest' defined from td_guest.xml
    virsh # list --all
    Id Name State
    ------------------------------
    - my-td-guest shut off
    virsh # start my-td-guest
    Domain 'my-td-guest' started
    virsh # console my-td-guest
    Connected to domain 'my-td-guest'
    …...
    virsh # quit

    ```

## Verify TDX

You can verify TDX from the booted guest VM console:  
If boot TDVM using qemu-kvm directly, login TDVM: ssh -p 10022 user@localhost  
If boot TDVM using virsh, login to TDVM in virsh console directly.  

Verify TDX is enabled in the guest.  

```bash
sudo dmesg | grep -i tdx
```

Example output:
```
[ 0.000000] tdx: Guest detected
```  

Verify the tdx_guest device exists  
```bash
ls /dev/tdx_guest 
```

Example output:
```
/dev/tdx_guest 
```

## Debug console

Optionally, enable serial port for the VM for more debug output by setting kernel cmdline parameters:

```bash
sudo grubby --update-kernel=ALL --args="console=hvc0 earlyprintk=ttyS0 3"
```
